#[0.8d] () (2018-11-20)

### 기능


* 주차권 이용 내역 

     스웨거 : 주차티켓 -> 주차권 이용 내역 (/v1/tickets/history, GET)
   
     제플린 : 39_history_parking
   
* 주차권 이용 내역 상세 
    
     스웨거 : 주차티켓 - 주차권 이용 내역 상세 (/v/tickets/history/{id}/{ticketType}, GET)    
                
     제플린 : 40_history_parking_detail, 19_parking_default_detail, 41_history_season_status_confirm
     
* 주차장 정기권 결제

      스웨거 : 주차장 -> 정기권 결제 (/v1/payment, POST)
        
      제플린 : 22_season_ticket_payment, 23_payment_confirm   

* 앱 버전 체크
   
      스웨거 : 버전관리 -> 앱 버전 체크 (/v1/app/version/check, POST)
    
      제플린 : 없음 (앱 실행시)


### 변경사항

* 주차장 상세보기 시간요금 리스트로 변경(pklTimePriceList)
   
       스웨거 : 주차장 -> 주차장 상세보기 (/v1/pkls/{id}, GET)
    
       제플린 : 25


#[0.8c]() (2018-11-16)
### 기능

* 주차장 정기권

    스웨거 : 주차장 -> 주차장 정기권 (/v1/pkls/{id}/season, GET)
        
    제플린 : 27_season_ticket_apply     
    
* 주차장 정기권 신청하기
    
    스웨거 : 정기권 -> 정기권 신청하기 (/v1/season-tickets/apply, POST)  
    
    제플린 : 27_season_ticket_apply, 28_season_ticket_apply_complete     
  



#[0.8b]() (2018-11-15)
### 기능

*자동결제 이용약관 

    스웨거 : 웹뷰 -> 자동결제 이용약관 (/v1/board/autopay, GET)
    
    제플린 : 37_payment_card_add
    
*전자금융거래 이용약관 

    스웨거 : 웹뷰 -> 자동결제 이용약관 (/v1/board/efinance, GET)
    
    제플린 : 37_payment_card_add
    
*개인정보 수집 및 이용안내 

    스웨거 : 웹뷰 -> 개인정보 수집 및 이용안내 (/v1/board/useinfo, GET)
    
    제플린 : 37_payment_card_add
    
*개인정보 수집 및 위탁안내

    스웨거 : 웹뷰 -> 개인정보 수집 및 위탁안내 (/v1/board/commissioninfo, GET)
    
    제플린 : 37_payment_card_add

*고유식별정보 수집 및 이용안내
    
    스웨거 : 웹뷰 -> 고유식별정보 수집 및 이용안내 (/v1/board/uniqueinfo, GET)
    
    제플린 : 37_payment_card_add
    
*결제 카드 이름 수정
    
    스웨거 : 결제 카드 -> 결제 카드 이름 수정 (/v1/payment-cards/{id}/name,PATCH)
    
    제플린 : 36_payment_card_edit
    
*결제 카드 주 카드 수정    
    
    스웨거 : 결제 카드 -> 결제 카드 이름 수정 (/v1/payment-cards/{id}/main,PATCH)
            
    제플린 : 36_payment_card_edit

*주차장 상세보기
   
    스웨거 : 주차장 -> 주차장 상세보기 (/v1/pkls/{id}, GET)
    
    제플린 : 24,25,26
    
### 버그수정
* 마이페이지 수정시 유저를 찾을 수 없습니다 나오던 문제 수정

#[0.8a]()(2018-11-13)
### 기능

*주차 요금 결제 

    스웨거 : 주차티켓 -> 주차 요금 결제 (/v1/tickets/{id}/payment,GET)
    
    제플린 : 15_parking_payment
    
*주차 요금 결제하기      
    
    스웨거 : 주차티켓 -> 주차 요금 결제하기 (/v1/tickets/{id}/payment,POST)
    
    제플린 : 15_parking_payment , 23_payment_confirm


#[0.8] (2018-11-12)
### 기능

*본인인증 SMS 요청    

   스웨거 : 계정 -> 본인인증 SMS 요청
   
   제플린 : 02_find_id_pw , 04_sign_up_form, 38~39 휴대전화 변경
   
*본인인증 SMS 확인
   
   스웨거 : 계정 -> 본인인증 SMS 확인 (본인인증 모듈 적용전까진 요청할때 받은 리턴값과 전화번호로 인증 확인 요청 )

   제플린 : 02_find_id_pw_입력 , 04_sign_up_form , 38~39 휴대전화 변경
   
*이메일 찾기 SMS 확인
   
   스웨거 : 계정 -> 이메일 찾기 SMS 문자 확인 
   
           (본인인증 모듈 적용전까진 요청할때 받은 리턴값과 전화번호로 인증 확인 요청 )
   
   제플린 : 02_find_id_pw_인증완료
   
*결제 카드 등록
   
   스웨거 : 결제카드 -> 결제 카드 등록 (/v1/payment-cards,POST) 
   
   제플린 : 37_payment_card_add  
    
*결제 카드 수정
   
   스웨거 : 결제카드 -> 결제 카드 등록 (/v1/payment-cards,PUT) 
   
   제플린 : ?

*결제 카드 목록   

    스웨거 : 결제카드 -> 결제 카드 목록 (/v1/payment-cards,GET) 
      
    제플린 : 36_payment_card_list
    
*결제 카드 삭제   

    스웨거 : 결제카드 -> 결제 카드 목록 (/v1/payment-cards/{id},DELETE) 
      
    제플린 : 36_payment_card_list_edit

* 주차 현황 상세보기
   
    스웨거 : 주차티켓 -> 주차 현황 상세보기 (/v1/tickets/{id}, GET)
    
    제플린 : 13_main_parking_condition
    
* 주차 현황 자동 정산 설정 

  스웨거 : 주차티켓 -> 주차 현황 자동 정산 설정 (/v1/tickets/{id}, PATCH) 
       
  제플린 : 13_main_parking_condition
     
   
   
#[0.7c](2018-11-01)

###기능
* 주차면 상세보기
   스웨거 : 주차면 -> 주차면 상세보기 (/v1/pkl-discretes/{id},GET)

   response
    
```javascript
    {
      "result": {
        "id": 1,
        "name": "영동대로96길(구)-0",
        "tagId": "31211103",
        "bluetoothId": "KSTBL-1495365-0",
        "parkingStatus": "EXIT",
        "pklId": 1,
        "parkingName": "영동대로96길(구)",
        "latitude": 37.5101962,
        "longitude": 127.06542933,
        "lora": true,
        "sensor": true,
        "bluetoothStatus": true,
        "impossible": false,
        "battery": 0
      },
      "resultCode": [
        "2000",
        "SUCCESS"
      ]
    }
```

* 회원삭제 : 회원 테스트용 회원 삭제
  스웨거 : 계정 -> 회원 삭제 (/v1/account, DELETE)

### 버그수정
* 소셜 로그인시 회원정보 안나오던 문제 수정
  스웨거 :  계정 -> 소셜 로그인 (/v1/account/social, POST)
   
 

#[0.7b]()(2018-10-25)

### 버그수정

* 푸시가 잘못날라가던 문제 수정


#[0.7a]()(2018-10-25)

### 버그수정

* 블루투스로 주차면 조회시 주차면이 없을경우 에러나던 문제 수정 
  
  스웨거 : 주차면 -> 블루투스로 주차면 조회 (/v1/pkl-discretes/bluetooth/{bluetoothId},GET)


# [0.7](https://bitbucket.org/kstparking/kst-parking-api-server/commits/tag/0.7)(2018-10-24)

* 주차장 검색  

  검색어로 주차장이름이나 주소 검색하여 주차장 반환

  스웨거 : 주차장 -> 주차장 검색 (/pkls/search/{search_word}, GET)
   
  제플린 : 10_search_filter
  
* 감면조건 목록 
   
  등록된 감면 조건 목록 
  
  스웨거 : 감면 -> 감면 목록 검색 (/remissions, GET)
     
  제플린 : 35_car_reduction_add
  
      
  Response
```javascript
  {
    "result": [
      {
        "id": 1,
        "name": "승용차 요일제 할인",
        "percent": 50
      },
      {
        "id": 2,
        "name": "이벤트 할인 티켓",
        "percent": 10
      }
    ],
    "resultCode": [
      "2000",
      "SUCCESS"
    ]
  }
```   
  
* 자동차 감면 등록

   스웨거 : 자동차 -> 자동차 감면 등록 (/cars/remissions, POST)
   
   제플린 : 35_car_reduction_add
   

* 차량 상세 에서  감면 조건 추가
  
  스웨거 : 차량상세    
  
  제플린 : 34_car_detail_add_reduction 
   
```javascript

    {
      "result": {
        "car": {
          "id": 1,
          "userId": 3,
          "carNumber": "32가1111",
          "carType": "SUB_COMPACT",
          "imageUrl": "https://ssproxy.ucloudbiz.olleh.com/v1/AUTH_36aca302-be65-43a8-92a7-ec06c410325d/KST/car/fdef68386c2b48aaa9c22cdd432f72e420181024011915.png"
        },
        "remission": {
          "id": 1,
          "name": "승용차 요일제 할인",
          "percent": 50,
          "price": 0,
          "remissionPhotos": [
            {
              "id": 1,
              "fileType": null,
              "fileExt": null,
              "filePath": "https://ssproxy.ucloudbiz.olleh.com/v1/AUTH_36aca302-be65-43a8-92a7-ec06c410325d/KST/carRemission/",
              "fileName": "1429f4b9294d449dab06999ff35269a120181024011802.png",
              "orgFileName": null,
              "contentsType": null,
              "fileSize": null,
              "carRemissionId": 2
            },
            {
              "id": 2,
              "fileType": null,
              "fileExt": null,
              "filePath": "https://ssproxy.ucloudbiz.olleh.com/v1/AUTH_36aca302-be65-43a8-92a7-ec06c410325d/KST/carRemission/",
              "fileName": "e3d8f90d2ee74d2f859011db1942cbc420181024011804.png",
              "orgFileName": null,
              "contentsType": null,
              "fileSize": null,
              "carRemissionId": 2
            }
          ],
          "remissionStatus": "JUDGE"
        }
      },
      "resultCode": [
        "2000",
        "SUCCESS"
      ]

```

# [0.6a]() (2018-10-23)

*  이미지 업로드시 일반 데이터 변경 (form -> query)

# [0.6](https://bitbucket.org/kstparking/kst-parking-api-server/commits/tag/0.6) (2018-10-23)

* 주차현황 

  스웨거 : 주차 티켓 -> 주차 현황 (/tickets, GET)
   
  제플린 : 14_main_parking_active

* 회원가입시 유저정보와 토큰정보 값 리턴 

* 자동차 등록시 이미지  추가 
  
  스웨거 : 자동차 -> 자동차 등록 (/cars, POST)
  
  request 타입 : Multipart Form
  
  request 예제 
 
  car={"carNumber": "string", "carType": "SUB_COMPACT"},file=@image


* 자동차 수정시 이미지 추가
  
  스웨거 : 자동차 -> 자동차 등록 (/cars, PUT)
  
  request 타입 : Multipart Form
  
  request 예제 
 
  car={"id":3,"carNumber": "11가1111", "carType": "SUB_COMPACT"},file=@image


* 회원가입시 약관 웹뷰 추가

   스웨거 : 웹뷰
  
    
   
  
### 버그 수정 

* 출차처리시 해당 주차 면에 하나이상의 기록이 있으면 서버에러 나던 문제 수정 

* 자동차 수정시 갱신 안되던 문제 수정

* 주차장 검색시 에러나던 문제 수정



# [0.5]()(2018-10-18)
###기능 
* 출차시 해당 차량에 푸쉬 정보 전송 

* 출차 감지 : 푸시 받은 정보로 출차 정보 조회

   스웨거 : 주차면 -> 출차 감지 (/pkl-discretes/exit, POST)

   제플린 : 16_main_parking_alert    
   

# [0.4](https://bitbucket.org/kstparking/kst-parking-api-server/commits/tag/0.4)(2018-10-17)
###기능 
* 블루투스 번호로 주차면 리스트 조회 
  
  테스트용 블루투스 아이디 : KSTBL-1495365-0 ~ KSTBL-1495365-80
  
  스웨거 : 주차면 -> 블루투스번호로 주차면 목록 조회 (/pkl-discretes/bluetooth/{bluetoothId}, GET) 
  
* 주차 확인

  스웨거 : 주차면 -> 주차 확인 (pkl-discretes/entrance, POST)
  
  제플린 : 12_main_parking_alert 주차확인  
  
### 버그 수정
* 주차장 목록 조회시 검색값 안넣으면 조회안되던 문제 수정 : 주차장 > 주차장 검색 (/pkls, GET) 
* 로그인, 소셜 로그인시 회원정보 포함하여 리턴  


# [0.3]()(2018-10-15) 
### 기능

* 소셜 로그인 추가 : 계정 > 소셜 회원 로그인 (/account/social
, POST)
* 소셜 회원가입 추가 : 계정 > 소셜 회원 가입 (/register/social, POST) 

##### 헤더에 토큰이 없거나 잘못된 토큰인 경우 등 인증되지 않은 접근시 에러코드 추가 
```javascript
{
  "resultCode": [
      "E9072",
      "인증되지 않은 접근 입니다."
  ]
}
```

#####  로그인 아이디 또는 비밀번호 잘못 입력시 에러코드 추가
```javascript
{
  "resultCode": [
    "E9073",
    "이메일 또는 비밀번호를 잘못 입력하셨습니다."
  ]
}

```
#####  이전비밀번호가 맞지 않을경우 에러코드 추가 (/account/change-password,POST)
```javascript
{
  "resultCode": [
    "E9046",
    "비밀번호가 맞지 않습니다."
  ]
}

```

* 소셜 Oauth 로그인 삭제  (/account/oauth)


### 버그 수정
* 유저 정보 (/account, GET) pushId, mobieOS 안나오던 문제 수정 



# [0.2]() (2018-10-10)
###기능 
* 200 리턴 값에 코드 부여 
```javascript
{
  "result": {
    "id_token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyQGxvY2FsaG9zdCIsImF1dGgiOiJST0xFX1VTRVIiLCJleHAiOjE1NDE2ODEwMTJ9.jg5V0gEbvC4_D2xBFBFX90_SKY2di8fz3PJFFQCuzPO53agIQTbq3xmF77ljga_0GABtJPeEngWDm1Pq8ttGtg"
  },
  "resultCode": [
    "2000",
    "SUCCESS"
  ]
}


{
  "resultCode": [
    "E9070",
    "이미 다른 고객께서 이메일을 사용하고 있습니다."
  ]
}
```

* SWAGGER 버전 변경 
* SWAGGER 주소 변경  : http://서버주소/swagger-ui.html
* SWAGGER 인증 버튼 추가 : 인증이 필요한 API 요청시 로그인 후 받은 토큰을 Bearer 토큰 으로 입력후 사용

### 버그 수정
* SWAGGER 작동안하던 API들 수정 


# [0.1]() (2018-10-8)

### Features

* 휴대폰번호 변경 추가 (/account/change-mobile)
* 비밀번호 변경 추가 (/account/change-password)
* 회원탈퇴 추가 (/account/close)

* 자동차 수정 추가 (/cars, put)
* 자동차 삭제 추가 (/cars, delete)
* 자동차 등록시 이름 항목 삭제 (/cars,POST)

* 스웨거 한글화 및 주석 추가 (진행중)

### Bug Fixes
* 회원정보 요청시 회원과 관련된 다른 정보들까지 넘어가던 문제 수정 (/account)
* 스웨거 문서 기능 이름이 영어로 나오던 문제 수정 (ex account-resource -> 계정)
* 스웨거 회원수정 이름 타입이 잘못되있던 문제 수정 
