# [0.6]() (2018-11-26)
### 기능 

* 관리자 주차장 상세 페이지 
  
  스웨거 : 어드민 주차장 -> 관리자 주차장 정보 페이지 (/admin/v1/pkls/{id}/info, GET)
    
  제플린 :  16_parking_lot_info
      
*관리자 주차면 주차현황
    
    스웨거 : 주차면 -> 주차 면 주차현황 (/admin/v1/pkl-discretes/{id},GET)
    
    제플린 : 17_parking_space_condition

*주차면 변경할 차량 목록 

    스웨거 : 주차면 -> 주차 면 변경할 차량 목록 (/admin/v1/pkl-discretes/{id}/cars,GET)
    
    제플린 : 18_park_space_car_add_edit

*입차 등록 및 변경 

    스웨거 : 주차면 -> 입차 등록 및 변경 (/admin/v1/pkl-discretes/{id}/{manual}/cars,GET)
            
            이미 주차면에 차량이 있으면 차량을 볼러옴
    
    제플린 : 18_park_space_car_add
    
*입차 등록 및 변경 실행하기

    스웨거 : 주차면 -> 입차 등록 및 변경 실행하기 (/admin/v1/pkl-discretes/car,POST)
            
            param : discreteId - 주차면 일련번호
            param : carId - 변경등록목록에서 가져온 자동차 아이디
            param : carNumber - 차량번호
            param : carType - 차량 유형
            param : enterTime - 차량 시간 (yyyy-MM-ddTHH:mm:ss)
            param : moveCarManual - 차량목록에서 가져온 자동차 수동등록여부 (true: 자동 false:수동)
            param : beforeDiscreteId  - 차량목록에서 가져온 자동차 주차면 번호 
            param : existingCarManual - 현재 주차중인 차량 수동등록여부 (true: 자동 false:수동)
    
    제플린 : 18_park_space_car_add

* 관리자 주차장 상세 페이지

    스웨거 : 어드민 주차장 -> 관리자 주차장 페이지 (/admin/v1/pkls/{id}, GET)    
    
    제플린 : 7_parking_main
    
* 고객민원 상세보기 api 이미지 정보 및 민원인 관련 정보 response parmeter 추가

    스웨거 : 고객 민원처리 -> 고객 민원 목록 (/admin/v1/claims/{id}, GET)
    
* 고객민원 목록 api 민원인 관련 정보 response parameter 추가

    스웨거 : 고객 민원처리 -> 고객 민원 목록 (/admin/v1/claims, GET)
    

# [0.5]() (2018-11-20)
* 앱 버전 목록
  
    스웨거  : 버전관리 -> 앱 버전 목록 (/admin/v1/app/version, GET)

* 앱 버전 등록

    스웨거  : 버전관리 -> 앱 버전 등록 (/admin/v1/app/version, POST)

* 앱 버전 수정

    스웨거  : 버전관리 -> 앱 버전 수정 (/admin/v1/app/version, PUT)

* 앱 버전 삭제

    스웨거  : 버전관리 -> 앱 버전 삭제 (/admin/v1/app/version/{id}, DELETE)

* 앱 버전 상세
    
    스웨거  : 버전관리 -> 앱 버전 상세 (/admin/v1/app/version/{id}, GET)

* 앱 버전 체크

    스웨거  : 버전관리 -> 앱 버전 체크 (/admin/v1/app/version/check, POST)


# [0.4]() (2018-11-12)
###기능 

* 어드민 업무 및 출퇴근 관리 리스트

   스웨거  : 근태관리 -> 근태내역 목록조회 (/admin/v1/attendance, GET)
   
* 출근등록

   스웨거  : 근태관리 -> 출근 등록 (/admin/v1/attendance/start, PUT)
   
* 퇴근등록

   스웨거  : 근태관리 -> 퇴근 등록 (/admin/v1/attendance/finish, PUT)
       
* 관리자 메인 페이지
    
    스웨거  : 어드민 주차장 -> 관리자 메인 페이지 (/admin/v1/pkl-main, GET)



# [0.3]() (2018-11-9)
###기능

* 어드민 비밀번호 변경 
   
   스웨거 : 계정 -> 비밀번호 변경 (/admin/v1/account/change-password, POST)
   
* 어드민 전화번호 변경 
   
   스웨거 : 계정 -> 전화번호 변경 (/admin/v1/account/change-mobile, POST)
   
* 어드민 비밀번호 찾기 
   
   스웨거 : 계정 -> 비밀번호 찾기 (/admin/v1/account/reset-password, POST)

* 어드민 계정 탈퇴 

   스웨거 : 계정 -> 계정 탈퇴 (/admin/v1/account/close, POST) 
   
   

# [0.2a]() (2018-11-8)
### 기능

* 어드민 정보

       스웨거 : 계정 -> 어드민 정보 (/admin/v1/account, GET)

# [0.2]() (2018-11-7)
### 기능 

* 공지사항 목록 

        스웨거 : 공지사항 -> 공지사항 목록 (/admin/v1/notices, get)


* 공지사항 상세보기 

          스웨거 : 공지사항 -> 공지사항 상세보기 (/admin/v1/notices/{공지사항번호}, get)  
         (email - admin@localhost pw - admin 으로 로그인시 테스트 데이터 사용가능)
  
* 고객 민원 처리 목록
    
        스웨거 : 고객 민원처리 -> 고객 민원 목록 (/admin/v1/claims, get)   
        (email - admin@localhost pw - admin 으로 로그인시 테스트 데이터 사용가능)
      
* 고객 민원 처리 상세보기
        
        스웨거 : 고객 민원처리 -> 고객 민원 목록 (/admin/v1/claims/{고객 민원 일련번호}, get)
         (email - admin@localhost pw - admin 으로 로그인시 테스트 데이터 사용가능)

* 알림 설정

        스웨거 : 알림 설정 -> 고객 알림 설정 (/admin/v1/alarm-setting,POST)

  
## 버그수정

* 회원가입시 어드민 코드 없던 문제 수정        

* 어드민 관리 주차장 목록에 위도, 경도 추가 



# [0.1]() (2018-10-30)

### 기능 

* 회원 가입

  스웨거 : 계정 -> 회원가입 (/admin/v1/register,POST) 

* 로그인 
   
  스웨거 : 인증 -> 로그인 (/admin/v1/authenticate, POST)

* 이메일 중복 체크

  스웨거 : 계정 -> 이메일 중복 체크 (/admin/v1/account/existing-email,POST)

* 주차장 검색

  스웨거 : 어드민 주차장 -> 주차장 검색 (/admin/v1/pkls, GET)
